# 4. Посчитайте, сколько раз символ встречается в строке.

def count_symbol_in_string(string, char):
    count = 0
    for i in range(len(string)):
        if string[i] == char:
            count += 1
    return count

string = " It inhabits brushy forest edges, coffee plantations and occasionally gardens at altitudes from 900 to 2,000 metres (3,000 to 6,600 ft), and up to 2,500 metres (8,200 ft) when not breeding."

print(count_symbol_in_string(string, 'e' ))
