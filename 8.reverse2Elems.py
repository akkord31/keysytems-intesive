#8. Напишите функцию reverse_my_list(lst), которая принимает список и меняет 
# местами его первый и последний элемент.

def reverse_my_list(lst):
    last_element = lst.pop()  
    first_element = lst.pop(0)  
    lst.append(first_element) 
    lst.insert(0, last_element)  
    return lst
 

print(reverse_my_list([1, 2, 3, 4, 5]))
print(reverse_my_list(['a', 'b', 'c', 'd', 'e']))