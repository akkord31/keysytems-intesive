#6. Написать функцию, которая сливает два словаря в один.
x = {'a': 1, 'b': 2}
y = {'b': 3, 'c': 4}

def merge_two_dicts(x, y):
    z = x.copy()   
    z.update(y)    
    return z

z = merge_two_dicts(x, y)  

print(z)