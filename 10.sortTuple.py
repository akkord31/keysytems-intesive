#10. Напишите функцию tuple_sort(), которая сортирует кортеж, 
# состоящий из целых чисел по возрастанию и возвращает его.

def tuple_sort(typle):
  temp_list = list(typle)
  for i in range(0, len(temp_list) - 1):
    for d in range(i + 1, len(temp_list)):
      if temp_list[i] > temp_list[d]:
        a: int = temp_list[i]
        temp_list[i] = temp_list[d]
        temp_list[d] = a
  return tuple(temp_list)


unsorted: tuple = (4, 3, 2, 1, 56, 4, 3, 2, 2)

print(tuple_sort(unsorted))

