#12. Написать функцию date, принимающую 3 аргумента — день, месяц и год.
#  Вернуть True, если такая дата есть в нашем календаре, и False иначе.

from datetime import date as date_func

def date(day,month,year):
  try:
    date_func(year,month,day)
    return 1
  except:
    return 0



print(bool(date(7,6,2022)))
print(bool(date(9999,-9999,9999)))