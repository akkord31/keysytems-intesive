
class Animal:
  def __init__(self, weight, length):
    self.weight = weight
    self.length = length

  def eat(self):
    print('Eating')

  def sleep(self):
    print('Sleeping')

class Bird(Animal):
  def __init__(self, weight, length, flying_speed):
    super().__init__(weight, length)
    self.flying_speed = flying_speed

  def move(self):
    print('Flying')

class Flightless(Bird):
  def __init__(self, running_speed):
    self.running_speed = running_speed

  def move(self):
    print('Running')

chicken = Flightless(60)
pigeon = Bird(0.3, 30, 100)